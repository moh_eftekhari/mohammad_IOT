import cherrypy 
import json

def string_reverse(string):
    #there is a python function that does this: reversed(string)
    return string[::-1] # [a:b:step] going from a to b using a step equal to step
class StringReverse(object):
    exposed = True
    def GET(self, *uri, **params):
        pass
        # output = ""
        # if (len(uri)!=0 and uri[0] == "ciao"):
        #     for pathi in uri:
        #         output += "<br>rev_uri: "+string_reverse(pathi)
        # else:
        #     raise cherrypy.HTTPError(404, "No path given.")
        
        # if params!={}:
        #     output += "<br>params: "+','.str(params)
        # return output
    def PUT(self, *uri, **params):
        body=cherrypy.request.body.read()
        if len(body)>0:
            try:
                jsonBody=json.loads(body)
                for key in jsonBody.keys():
                    jsonBody[key]= string_reverse(jsonBody[key])
                return json.dumps(jsonBody)
            except json.decoder.JSONDecodeError:
                raise cherrypy.HTTPError(400,"Bad Request. Body must be a valid JSON")
            except:
                raise cherrypy.HTTPError(500,"Internal Server Error")
        else:
            return "Empty Body"
    
if __name__ == '__main__':
    conf = {
        '/':{
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on': True,
        }
    }
    webservice = StringReverse()
    cherrypy.tree.mount(webservice, '/', conf)
    cherrypy.config.update({'server.socket_port': 9090})
    # cherrypy.config.update({'server.socket_host': '0,0,0,0'})
    cherrypy.engine.start()
    cherrypy.engine.block()