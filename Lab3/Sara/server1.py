import requests
import json
import cherrypy
from client1 import clientClass
class calculator:
    exposed = True
    # No init func needed since data is dynamic, There is no point in storing them in your func
    def write_to_json(self, dict):
        with open('my_file.json', 'w') as file:
            json.dump(dict, file, indent=4)
    # def GET(self, *uri, **params):
    def GET(self, *path, **params):
        dict = {'operand': None, 'op1': None, 'op2': None, 'Result': None}
        # init method is not for handling HTTP request parameters. use Get, Put, Post or Del to work with params

        operand = path[0]
        op1 = int(params['op1'])
        op2 = int(params['op2'])
        if operand == 'add':
            dict['operand'] = 'add'
            result = op1 + op2

        elif self.operand == 'sub':
            dict['operand'] = 'sub'
            result = op1 - op2

        elif self.operand == 'mul':
            dict['operand'] = 'mul'
            result = op1 * op2

        elif self.operand == 'div':
            if dict['op2'] == 0:
                raise cherrypy.HTTPError(500, "Devision by zero")
            dict['operand'] = 'div'
            result = op1 / op2

        dict['op1'] = op1
        dict['op2'] = op2
        dict['Result'] = result
        self.write_to_json(dict)
        return json.dumps(dict)

if __name__ == "__main__":
    conf = {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on': True
        }
    }
    webService = calculator()
    cherrypy.tree.mount(webService, '/', conf)
    cherrypy.config.update({'server.socket_port': 8080})
    cherrypy.engine.start()
    cherrypy.engine.block()