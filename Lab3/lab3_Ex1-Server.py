import json

class Calculator:
    def __init__(self, param1, param2):
        self.param1 = param1
        self.param2 = param2
        self.result = None
        self.operation = None
        self.result_json = {"Operation": self.operation, "Operand 1": self.param1, "Operand 2": self.param2, "Result": self.result}
    
    def add(self):
        self.result = self.param1 + self.param2
        self.result_json["Result"] = self.result
        self.result_json["Operation"] = "add"
        return self.result
    def sub(self):
        self.result = self.param1 - self.param2
        self.result_json["Result"] = self.result
        self.result_json["Operation"] = "sub"
        return self.result
    def mul(self):
        self.result = self.param1 * self.param2
        self.result_json["Result"] = self.result
        self.result_json["Operation"] = "mul"
        return self.result
    def div(self):
        if self.param2 == 0:
            print("Division by zero is not allowed")
        elif self.param2 != 0:
            self.result = self.param1 / self.param2
            self.result_json["Result"] = self.result
            self.result_json["Operation"] = "div"
            return self.result
    
if __name__ == "__main__":
    
    while True:
        operation = input("Enter operation: add/sub/mul/div/exit: ")
        if operation == "exit":
            print("Calculator closed.")
            break
        elif operation not in ["add", "sub", "mul", "div"]:
            print("Invalid operation. Please enter a valid operation.")
            continue
        param1 = float(input("Enter first operand: "))
        param2 = float(input("Enter second operand: "))
        mycal = Calculator(param1, param2)
        if operation == "add":
            mycal.add()
            print(json.dumps(mycal.result_json, indent=4))
        elif operation == "sub":
            mycal.sub()
            print(json.dumps(mycal.result_json, indent=4))
        elif operation == "mul":
            mycal.mul()
            print(json.dumps(mycal.result_json, indent=4))
        elif operation == "div":
            mycal.div()
            print(json.dumps(mycal.result_json, indent=4))
    
    
    