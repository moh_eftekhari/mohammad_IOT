import cherrypy
        
class calculator(object):
    exposed=True
    def GET(self,*uri,**params):
        #Standard output
        output="Hello World"
        #Check the uri in the requests
        #<br> is just used to append the content in a new line
        #(<br> is the \n for HTML)
        if len(uri)!=0:
            output+='<br>uri: '+','.join(uri)
        #Check the parameters in the request
        #<br> is just used to append the content in a new line
        #(<br> is the \n for HTML)
        if params!={}:
            output+='<br>params: '+str(params)
        return output
        
        #string_to_return="Hello world! <br>"
        #string_to_return+=str(uri)+"<br>"
        #string_to_return+=str(params)
        #return string_to_return
    def POST(self,*uri,**params):
        pass
    def PUT(self,*uri,**params):
        pass
    def DELETE(self,*uri,**params):
        pass

if __name__ == '__main__':
        
    conf={
        '/':{
            'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on':True
            }
        }
cherrypy.tree.mount(calculator(),'/',conf)
cherrypy.engine.update({'server.socket_port':8080})
cherrypy.engine.update({'server.socket_host':'0.0.0.0'})
cherrypy.engine.start()
cherrypy.engine.block()