import random
import string
from typing import Any
import cherrypy

class StringGeneratorWebservice(object):
    def GET (self, *path, **query): 
        if (len(path)>0 and path[0] == "ciao"):
            return ("URI: %s; Parameters %s" % (str (path), str(query)))
        else:
            raise cherrypy.HTTPError(404, "Error message")
    def POST (self, *path, **query): 
        pass
    def PUT (self, *path, **query): 
        pass    
    def DELETE (self, *path, **query):
        pass
class OtherWebService(object):
     pass
    # def POST(self, *path, **query):
    #     mystring = "Post Response" 
    #     my_string = cherrypy.request.body.read()
    #     return my_string
    
    # def PUT(self, *path, **query):
    #     mystring = "Put Response" 
    #     my_string = cherrypy.request.body.read()
    #     return my_string
    
if __name__ == '__main__': 
        conf = { 
                '/': { 
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(), 
                'tools.sessions.on': True, 
                } 
        }
         
cherrypy.tree.mount (StringGeneratorWebservice(), '/string', conf)
cherrypy.tree.mount (OtherWebService(), '/other', conf)
# cherrypy.config.update({'server.socket_host': '0.0.0.0'})
cherrypy.config.update({'server.socket_port': 8080})
cherrypy.engine.start() 
cherrypy.engine.block()