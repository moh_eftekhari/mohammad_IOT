import json
from datetime import datetime

catalog_file = "catalog.json"

def load_catalog():
    try:
        with open(catalog_file, 'r') as f:
            catalog = json.load(f)
            return catalog
    except FileNotFoundError:
        return []
    
def save_catalog(catalog):
    with open(catalog_file, 'w') as f:
        json.dump(catalog, f)

def print_device_info(device):
    print("\nDevice Information:")
    for key, value in device.items():
        print(f"{key}: {value}") 

def searchByName(catalog, name):
    for dname in catalog:
        if dname['deviceName'] == name:
            return dname
    print_device_info(dname)

def searchById(catalog, deviceID):
    for item in catalog:
        if item['deviceID'] == deviceID:
            return item
    print_device_info(item)

def searchByService():
    pass




if __name__ == "__main__":
    catalog = load_catalog()

    while True:
        print("\nOptions:")
        print("1. Search by Name")
        print("2. Search by ID")
        print("3. Search by Service")
        print("4. Search by Measure Type")
        print("5. Insert Device")
        print("6. Print All Devices")
        print("7. Exit")

        choice = input("Enter your choice: ")

        if choice=="1":
            name = input("Enter device name: ")
            searchByName(catalog, name)
        elif choice=="2":
            deviceID = input("Enter device ID: ")
            searchById(catalog, deviceID)
        elif choice == "7":
            save_catalog(catalog)
            print("Catalog saved. Exiting.")
            break
        else:
            print("Invalid choice. Please enter a valid option.")