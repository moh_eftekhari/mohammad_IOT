import json

class Calculator:
    def __init__(self, paramList):
        self.paramlist = paramList
        self.result = None
        self.operation = None
        self.result_json = {"Operation": self.operation, "Operands": self.paramlist, "Result": self.result}
    
    def add(self):
        self.result = self.paramlist[0]
        for i in self.paramlist[1:]:
            self.result += i
        self.result_json["Result"] = self.result
        self.result_json["Operation"] = "add"
        return self.result
    def sub(self):
        self.result = self.paramlist[0]
        for i in self.paramlist[1:]:
            self.result -= i
        self.result_json["Result"] = self.result
        self.result_json["Operation"] = "sub"
        return self.result
    def mul(self):
        self.result = self.paramlist[0]
        for i in self.paramlist[1:]:
            self.result *= i
        self.result_json["Result"] = self.result
        self.result_json["Operation"] = "mul"
        return self.result
    def div(self):
        if 0 in self.paramlist:
            print("Cannot divide by 0.")
        
        self.result = self.paramlist[0]
        for i in self.paramlist[1:]:
            self.result /= i
        self.result_json["Result"] = round(self.result,2)
        self.result_json["Operation"] = "div"
        return self.result
    
if __name__ == "__main__":

    while True:
        operation = input("Enter operation: add/sub/mul/div/exit: ")
        if operation == "exit":
            print("Calculator closed.")
            break
        elif operation not in ["add", "sub", "mul", "div"]:
            print("Invalid operation. Please enter a valid operation.")
            continue
        paramList = input("Enter number with space: ").split(" ")
        
        if "" in paramList:
            print("Invalid input. Please enter a valid number.")    
            continue

        for i in range(len(paramList)):
            paramList[i] = float(paramList[i])
        mycal = Calculator(paramList)
        if operation == "add":
            mycal.add()
            print(json.dumps(mycal.result_json, indent=4))
        elif operation == "sub":
            mycal.sub()
            print(json.dumps(mycal.result_json, indent=4))
        elif operation == "mul":
            mycal.mul()
            print(json.dumps(mycal.result_json, indent=4))
        elif operation == "div":
            mycal.div()
            print(json.dumps(mycal.result_json, indent=4))
    
    
    